## Processing code 

The processing code has been sorted into 4 folders according to usage. The library functions are used in all processing implementations. Once downloaded the code can be stored in either one of two folder architectures.
 
*   As presented here. Add the parent folder, with subfolders, to the Matlab path. 
*   All the functions can be put into one folder and that folder added to the Matlab path.

For detailed instructions on code usage see the respective coastal_implementation and global_implementation pages.
